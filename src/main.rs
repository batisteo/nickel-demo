#[macro_use] extern crate nickel;

use std::collections::HashMap;

use nickel::{Nickel, HttpRouter};

fn main() {
    let mut server = Nickel::new();

        server.get("/:user", middleware! { |request, response|
            let mut data = HashMap::new();
            data.insert("name", request.param("user"));
            return response.render("assets/template.tpl", &data);
        });

    server.listen("127.0.0.1:6767")
}
